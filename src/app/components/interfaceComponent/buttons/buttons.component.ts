import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'nutri-buttons',
  templateUrl: './buttons.component.html',
  styleUrls: ['./buttons.component.scss'],
})
export class ButtonsComponent implements OnInit {
  @Input() title: string = 'Button';

  constructor() {}

  ngOnInit(): void {}
}
