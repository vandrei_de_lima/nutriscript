import { ButtonsComponent } from './buttons/buttons.component';
import { HeaderComponent } from './header/header.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [HeaderComponent, ButtonsComponent],
  imports: [CommonModule],
  exports: [HeaderComponent, ButtonsComponent],
})
export class InterfaceComponentModule {}
